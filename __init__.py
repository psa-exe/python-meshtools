# -*- coding: utf-8 -*-
"""\
==============================
Tools for 3D mesh manipulation
==============================
"""

__author__ = 'Pedro Asad'
__copyright__ = 'Copyright 2016, UFRJ'
__credits__ = ['Pedro Asad']

__license__ = 'Creative Commons (CC BY 3.0)'
__version__ = '0.1.0.0'
__maintainer__ = 'Pedro Asad'
__email__ = 'pasad@cos.ufrj.br'
__status__ = 'development'
