#! /usr/bin/env python2
# -*- coding: utf-8 -*-
"""\
==========================================
Best-fitting rigid alignment of point sets
==========================================

Given two meshes in PLY format, computes and applies the rigid transformation
that best aligns the first with the second. It is necessary that both input
files contain the same number of vertices and that both  sequences are
specified in the same order. It could be used, for instance, for aligning two
versions of an object, where one of them has been slightly deformed. Run this
module with the '-h' switch to get help information.

Based on Chapter 4 of the book
    A Sampler of Useful Computational Tools for Applied Geometry, Computer
    Graphics and Image Processing
and the related paper
    Least-Squares Rigid Motion Using SVD.
"""

import logging as log
import numpy as np
import numpy.linalg as npl
import sys

from argparse import ArgumentParser, FileType


class PlyMesh(object):
    """A simple implementation of text ply file reading/writing.

    Some assumptions about the format of the input file are made. Read the code
    for more details.
    """

    def __init__(self, nverts, nfaces, vattribs):
        dtype = [(attrib, np.float) for attrib in vattribs]
        self.verts = np.zeros(nverts, dtype)
        self.faces = np.empty(nfaces, np.object)

    def __getitem__(self, name):
        """TODO: This method is only here for compatibility with the Plyfile
        library (which is not used anymore) and may be discarded in the near
        future.
        """
        if name == 'vertex':
            return self.verts
        elif name == 'face':
            return self.faces
        else:
            message = "%s instance has no '%s' attribute"
            values = (self.__class__.__name__, name)
            raise TypeError(message % values)

    @classmethod
    def read(Class, path):
        with open(path) as f:
            vattribs = []
            nverts = 0
            nfaces = 0

            line = ''
            while 'element' not in line:
                line = f.readline()
            nverts = int(line.split()[-1])

            line = f.readline()
            while 'element' not in line:
                vattribs.append(line.split()[-1])
                line = f.readline()
            nfaces = int(line.split()[-1])

            while 'end_header' not in line:
                line = f.readline()

            mesh = Class(nverts, nfaces, vattribs)

            for i in range(nverts):
                line = f.readline()
                mesh.verts[i] = tuple(float(s) for s in line.split())

            for i in range(nfaces):
                line = f.readline()
                mesh.faces[i] = [int(s) for s in line.split()][1:]

            return mesh
        return None

    def write(self, path):
        with open(path, 'w') as f:
            f.writelines([
                'ply\n',
                'format ascii 1.0\n',
                'element vertex %d\n' % len(self.verts)
            ])
            for field in self.verts.dtype.names:
                f.write('property float %s\n' % field)
            f.writelines([
                'element face %d\n' % len(self.faces),
                'property list uchar uint vertex_indices\n',
                'end_header\n'
            ])
            for row in self.verts:
                f.write(' '.join(['%.6f' % s for s in row]) + '\n')
            for row in self.faces:
                line = ['%d' % len(self.faces)] + ['%d' % s for s in row]
                f.write(' '.join(line) + '\n')

if __name__ == '__main__':
    parser = ArgumentParser(description=__doc__)

    parser.add_argument(
        'input1',
        type=FileType('r'), metavar='<input1>.ply',
        help='Path to first input PLY file (the one that will be transformed).'
    )

    parser.add_argument(
        'input2',
        type=FileType('r'), metavar='<input2>.ply',
        help='Path to second input PLY file (the reference mesh).'
    )

    parser.add_argument(
        'output',
        type=FileType('w'), metavar='<output>.ply',
        help='Path to output PLY file (result of transformation).'
    )

    parser.add_argument(
        '--verbose', '-v',
        action='count',
        help='Display execution info. Double to get debugging info.'
    )

    args = parser.parse_args()
    args.verbose = max(0, min(args.verbose, 2))

    # =========================================================================
    # Logging setup
    # =========================================================================
    loglevels = [log.WARN, log.INFO, log.DEBUG]
    log.basicConfig(stream=sys.stderr, level=loglevels[args.verbose])

    # =========================================================================
    log.info('Loading input meshes')
    # =========================================================================
    mesh1 = PlyMesh.read(args.input1.name)
    mesh2 = PlyMesh.read(args.input2.name)

    vert1 = np.vstack((mesh1['vertex'][c] for c in 'xyz'))
    vert2 = np.vstack((mesh2['vertex'][c] for c in 'xyz'))

    # =========================================================================
    log.info('Computing centroids and covariance matrix')
    # =========================================================================
    mean1 = np.mean(vert1, axis=1)
    mean2 = np.mean(vert2, axis=1)

    dmean1 = vert1 - mean1[:, np.newaxis]
    dmean2 = vert2 - mean2[:, np.newaxis]

    cov12 = np.dot(dmean1, dmean2.T)

    # =========================================================================
    log.info('Obtaining optimal transformation through SVD')
    # =========================================================================
    U, S, V = npl.svd(cov12)
    R = np.dot(
        np.dot(
            V.T,
            np.diag([1, 1, npl.det(np.dot(V.T, U.T))])
        ), U.T
    )
    t = mean2 - np.dot(R, mean1)

    # =========================================================================
    log.info('Transforming vertices in original mesh')
    # =========================================================================
    vert1 = np.dot(R, vert1) + t[:, np.newaxis]

    mesh1['vertex']['x'] = vert1[0, :]
    mesh1['vertex']['y'] = vert1[1, :]
    mesh1['vertex']['z'] = vert1[2, :]

    # =========================================================================
    log.info('Saving output mesh')
    # =========================================================================
    mesh1.write(args.output.name)

    # =========================================================================
    log.info('Done, bye!')
    # =========================================================================
